package com.example.demo.database_query_providers;

import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import org.apache.ibatis.jdbc.SQL;

public class BookQueryProvider
{
    //GET ALL BOOKs
    public String getAllBooks()
    {
        return new SQL(){{
            SELECT("*");
            FROM("tb_book");
        }}.toString();
    }


    //INSERT NEW BOOK
    public String insertNewBook()
    {
        return new SQL(){{
            INSERT_INTO("tb_book");
            VALUES("title", "#{title}");
            VALUES("author", "#{author}");
            VALUES("description", "#{description}");
            VALUES("thumbnail", "#{thumbnail}");
            VALUES("category_id", "#{categoryId}");
        }}.toString();
    }


    //DELETE BOOK BY ID
    public String deleteBookByID()
    {
        return new SQL(){{
            DELETE_FROM("tb_book");
            WHERE("id = #{id}");
        }}.toString();
    }


    //UPDATE BOOk
    public String updateBookByID()
    {
//        return new SQL(){{
//            UPDATE("tb_book");
//            SET("book_id = #{id}, "+
//                        "title = #{title}, " +
//                    "author = #{author}, " +
//                    "description = #{description}, " +
//                    "thumbnail = #{thumbnail}, " +
//                    "category_id = #{categoryId} ");
//            WHERE("id = #{id}");
//        }}.toString();
        return new SQL(){{
            UPDATE("tb_book");
            SET("title = #{title}");
            SET("author = #{author}");
            SET("description = #{description}");
            SET("thumbnail = #{thumbnail}");
            SET("category_id = #{categoryId}");
            WHERE("id = #{id}");
        }}.toString();
    }

    //FIND A BOOK BY ID
    public String findBookByID()
    {
        return new SQL(){{
            SELECT("*");
            FROM("tb_book");
            WHERE("id = #{id}");
        }}.toString();
    }


    //FILTER BOOK BY CATEGORY
    public String filterBookByCategory()
    {
        return new SQL(){{
            SELECT("*");
            FROM("tb_book");
            WHERE("category_id = #{categoryId}");
        }}.toString();
    }


    //FILTER BOOK BY CATEGORY
    public String filterBookByTitle()
    {
        return new SQL(){{
            SELECT("*");
            FROM("tb_book");
            WHERE("title = #{title}");
        }}.toString();
    }


}
