package com.example.demo.database_query_providers;

import com.example.demo.models.category_model.CategoryRequest;
import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.jdbc.SQL;

public class CategoryQueryProvider
{
    //GET ALL CATEGORIES ==================
    public String selectAllCategory()
    {
        return new SQL(){{
            SELECT("*");
            FROM("tb_category");
        }}.toString();
    }


    //INSERT NEW CATEGORY
    public String insertNewCategory()
    {
        return new SQL(){{
            INSERT_INTO("tb_category");
            VALUES("title", "#{title}");
        }}.toString();
    }


    //DELETE CATEGORY BY ID
    public String deleteCategoryByID()
    {
        return new SQL(){{
            DELETE_FROM("tb_category");
            WHERE("id = #{id}");
        }}.toString();
    }


    //UPDATE CATEGORY BY ID
    public String updateCategoryByID() {
        return new SQL() {{
            UPDATE("tb_category");
            SET("title = #{title}");
            WHERE("id = #{id}");
        }}.toString();
    }
}
