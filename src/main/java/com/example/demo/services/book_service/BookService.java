package com.example.demo.services.book_service;

import com.example.demo.models.book_model.BookRequest;
import com.example.demo.models.book_model.BookResponse;

import java.util.List;

public interface BookService
{
    List<BookResponse> getAllBooks();
    boolean insertNewBook(BookRequest bookRequest);
    boolean deleteBookByID(int id);
    boolean updateBookByID(int id, BookRequest bookRequest);
    BookResponse getBookByID(int id);
    List<BookResponse> filterBookByCategory(int categoryId);
}
