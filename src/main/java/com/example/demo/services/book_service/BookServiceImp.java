package com.example.demo.services.book_service;

import com.example.demo.models.book_model.BookRequest;
import com.example.demo.models.book_model.BookResponse;
import com.example.demo.repositories.book_repository.BookRepoImp;
import com.example.demo.repositories.book_repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImp implements BookService{
    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<BookResponse> getAllBooks() {
        return bookRepository.getAllBooks();
    }

    @Override
    public boolean insertNewBook(BookRequest bookRequest) {
        return bookRepository.insertNewBook(bookRequest);
    }

    @Override
    public boolean deleteBookByID(int id) {
        return bookRepository.deleteBookByID(id);
    }

    @Override
    public boolean updateBookByID(int id, BookRequest bookRequest) {
        return bookRepository.updateBookByID(id,bookRequest.getTitle(), bookRequest.getAuthor(),
                bookRequest.getDescription(),bookRequest.getThumbnail(),bookRequest.getCategoryId());
    }

    @Override
    public BookResponse getBookByID(int id) {
        return bookRepository.getBookByID(id);
    }

    @Override
    public List<BookResponse> filterBookByCategory(int categorryId) {
        return bookRepository.filterBookByCategory(categorryId);
    }
}
