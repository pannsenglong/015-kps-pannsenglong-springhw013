package com.example.demo.services.category_service;

import com.example.demo.models.category_model.CategoryRequest;
import com.example.demo.models.category_model.CategoryResponse;

import java.util.List;

public interface CategoryService
{
    List<CategoryResponse> getAllCategory();
    CategoryRequest insertNewCategory(CategoryRequest categoryRequest);
    boolean deleteCategoryByID(int id);
    boolean updateCategoryByID(int id,CategoryRequest categoryRequest);
}
