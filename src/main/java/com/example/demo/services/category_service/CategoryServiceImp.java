package com.example.demo.services.category_service;

import com.example.demo.models.category_model.CategoryRequest;
import com.example.demo.models.category_model.CategoryResponse;
import com.example.demo.repositories.category_repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService{

    private CategoryRepository categoryRepo;

    @Autowired
    public void setCategoryRepo(CategoryRepository categoryRepo)
    {
        this.categoryRepo = categoryRepo;
    }

    @Override
    public List<CategoryResponse> getAllCategory() {
        return categoryRepo.getAllCategory();
    }

    @Override
    public CategoryRequest insertNewCategory(CategoryRequest categoryRequest) {
        boolean isInserted = categoryRepo.insertNewCategory(categoryRequest);
        if(isInserted)
            return categoryRequest;
        return null;
    }

    @Override
    public boolean deleteCategoryByID(int id) {
        return categoryRepo.deleteCategoryByID(id);
    }


    @Override
    public boolean updateCategoryByID(int id,CategoryRequest categoryRequest) {
        return categoryRepo.updateCategoryByID(id,categoryRequest.getTitle());
    }
}
