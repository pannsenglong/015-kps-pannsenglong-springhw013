package com.example.demo.controllers;

import com.example.demo.models.BaseApiResponse.BaseApiDelete_UpdateResponse;
import com.example.demo.models.BaseApiResponse.BaseApiResponse;
import com.example.demo.models.book_model.BookRequest;
import com.example.demo.models.book_model.BookResponse;
import com.example.demo.models.category_model.CategoryRequest;
import com.example.demo.models.category_model.CategoryResponse;
import com.example.demo.repositories.book_repository.BookRepoImp;
import com.example.demo.repositories.book_repository.BookRepository;
import com.example.demo.services.book_service.BookServiceImp;
import org.apache.ibatis.annotations.SelectProvider;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import java.rmi.server.UID;
import java.sql.Timestamp;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class BookController
{

    private BookServiceImp bookServiceImp;

    @Autowired
    public void setBookServiceImp(BookServiceImp bookServiceImp) {
        this.bookServiceImp = bookServiceImp;
    }


    @GetMapping("/books")
    public ResponseEntity<List<BookResponse>> getAll()
    {
        String path = "/api/v1/books";
        BaseApiResponse baseApiResponse = new BaseApiResponse();
        baseApiResponse.setMessage("Get all Books successfully.");
        baseApiResponse.setData(bookServiceImp.getAllBooks());
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        baseApiResponse.setPath(path);
        return new ResponseEntity(baseApiResponse,HttpStatus.OK);
    }


    @PostMapping("/book")
    public ResponseEntity<BookRequest> insert(@RequestBody BookRequest request)
    {
        String path = "/api/v1/category";
        ModelMapper mapper = new ModelMapper();
        BaseApiResponse baseApiResponse = mapper.map(request, BaseApiResponse.class);
        baseApiResponse.setMessage("New Book has been added successfully.");
        baseApiResponse.setData(request);
        baseApiResponse.setStatus(HttpStatus.CREATED);
        baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        bookServiceImp.insertNewBook(request);
        baseApiResponse.setPath(path);
        return new ResponseEntity(baseApiResponse, HttpStatus.CREATED);
    }

    @DeleteMapping("/book/delete/{id}")
    public ResponseEntity<BookResponse> delete(@RequestBody @PathVariable int id)
    {
        if (bookServiceImp.deleteBookByID(id))
        {
            String path = "/api/v1/book/delete/"+id;
            BaseApiDelete_UpdateResponse baseApiResponse = new BaseApiDelete_UpdateResponse();
            baseApiResponse.setMessage("One book has been removed successfully.");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
            baseApiResponse.setPath(path);
            return new ResponseEntity(baseApiResponse, HttpStatus.OK);
        }
        else
        {
            String path = "/api/v1/book/delete/"+id;
            BaseApiDelete_UpdateResponse baseApiResponse = new BaseApiDelete_UpdateResponse();
            baseApiResponse.setMessage("Failed to remove book.");
            baseApiResponse.setStatus(HttpStatus.NOT_FOUND);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
            baseApiResponse.setPath(path);
            return new ResponseEntity(baseApiResponse, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/book/update/{id}")
    public ResponseEntity<BookRequest> update( @PathVariable int id,@RequestBody BookRequest bookRequest)
    {
        if (bookServiceImp.updateBookByID(id, bookRequest))
        {
            String path = "/api/v1/book/update/"+id;
            BaseApiDelete_UpdateResponse baseApiResponse = new BaseApiDelete_UpdateResponse();
            baseApiResponse.setMessage("Book has been updated successfully.");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
            baseApiResponse.setPath(path);
            return new ResponseEntity(baseApiResponse, HttpStatus.OK);

        }
        else
        {
            String path = "/api/v1/book/update/"+id;
            BaseApiDelete_UpdateResponse baseApiResponse = new BaseApiDelete_UpdateResponse();
            baseApiResponse.setMessage("Failed to update book.");
            baseApiResponse.setStatus(HttpStatus.NOT_FOUND);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
            baseApiResponse.setPath(path);
            return new ResponseEntity(baseApiResponse, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/book/{id}")
    public ResponseEntity<BookResponse> findBookByID( @PathVariable int id)
    {
        String path = "/api/v1/book/" + id;
        BaseApiResponse baseApiResponse = new BaseApiResponse();
        baseApiResponse.setMessage("Get Book successfully.");
        baseApiResponse.setData(bookServiceImp.getBookByID(id));
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        baseApiResponse.setPath(path);
        return new ResponseEntity(baseApiResponse,HttpStatus.OK);
    }

    @GetMapping("/book/category/{id}")
    public ResponseEntity<BookResponse> filterBookByCategory( @PathVariable int id)
    {
        String path = "/api/v1/book/category" + id;
        BaseApiResponse baseApiResponse = new BaseApiResponse();
        baseApiResponse.setMessage("Get Book successfully.");
        baseApiResponse.setData(bookServiceImp.filterBookByCategory(id));
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        baseApiResponse.setPath(path);
        return new ResponseEntity(baseApiResponse,HttpStatus.OK);
    }

}
