package com.example.demo.controllers;

import com.example.demo.models.BaseApiResponse.BaseApiDelete_UpdateResponse;
import com.example.demo.models.BaseApiResponse.BaseApiResponse;
import com.example.demo.models.category_model.CategoryRequest;
import com.example.demo.models.category_model.CategoryResponse;
import com.example.demo.services.category_service.CategoryServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;


import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CategoryController
{
    private CategoryServiceImp categoryServiceImp;

    @Autowired
    public void setCategoryServiceImp(CategoryServiceImp categoryServiceImp) {
        this.categoryServiceImp = categoryServiceImp;
    }

    @GetMapping("/categories")
    public ResponseEntity<List<CategoryResponse>> getAll()
    {
        String path = "/api/v1/category";
        BaseApiResponse baseApiResponse = new BaseApiResponse();
        baseApiResponse.setMessage("Get all categories successfully.");
        baseApiResponse.setData(categoryServiceImp.getAllCategory());
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        baseApiResponse.setPath(path);
        return new ResponseEntity(baseApiResponse,HttpStatus.OK);
    }


    @PostMapping("/category")
    public ResponseEntity<CategoryRequest> insert(@RequestBody CategoryRequest request)
    {
        String path = "/api/v1/category";
        ModelMapper mapper = new ModelMapper();
        BaseApiResponse baseApiResponse = mapper.map(request, BaseApiResponse.class);
        baseApiResponse.setMessage("New category has been added successfully.");
        baseApiResponse.setData(request);
        baseApiResponse.setStatus(HttpStatus.CREATED);
        baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        categoryServiceImp.insertNewCategory(request);
        baseApiResponse.setPath(path);
        return new ResponseEntity(baseApiResponse, HttpStatus.CREATED);
    }

    @DeleteMapping("/category/delete/{id}")
    public ResponseEntity<CategoryResponse> delete(@RequestBody @PathVariable int id)
    {
        if (categoryServiceImp.deleteCategoryByID(id))
        {
            String path = "/api/v1/category/delete/"+id;
            BaseApiDelete_UpdateResponse baseApiResponse = new BaseApiDelete_UpdateResponse();
            baseApiResponse.setMessage("One category has been removed successfully.");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
            baseApiResponse.setPath(path);
            return new ResponseEntity(baseApiResponse, HttpStatus.OK);
        }
        else
        {
            String path = "/api/v1/category/delete/"+id;
            BaseApiDelete_UpdateResponse baseApiResponse = new BaseApiDelete_UpdateResponse();
            baseApiResponse.setMessage("Failed to remove category.");
            baseApiResponse.setStatus(HttpStatus.NOT_FOUND);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
            baseApiResponse.setPath(path);
            return new ResponseEntity(baseApiResponse, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/category/update/{id}")
    public ResponseEntity<CategoryResponse> update( @PathVariable int id,@RequestBody CategoryRequest categoryRequest)
    {
        if (categoryServiceImp.updateCategoryByID(id, categoryRequest))
        {
            String path = "/api/v1/category/update/"+id;
            BaseApiDelete_UpdateResponse baseApiResponse = new BaseApiDelete_UpdateResponse();
            baseApiResponse.setMessage("One category has been updated successfully.");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
            baseApiResponse.setPath(path);
            return new ResponseEntity(baseApiResponse, HttpStatus.OK);

        }
        else
        {
            String path = "/api/v1/category/update/"+id;
            BaseApiDelete_UpdateResponse baseApiResponse = new BaseApiDelete_UpdateResponse();
            baseApiResponse.setMessage("Failed to update category.");
            baseApiResponse.setStatus(HttpStatus.NOT_FOUND);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
            baseApiResponse.setPath(path);
            return new ResponseEntity(baseApiResponse, HttpStatus.BAD_REQUEST);
        }
    }




}
