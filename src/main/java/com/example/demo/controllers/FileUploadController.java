package com.example.demo.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

@RestController
public class FileUploadController
{
    @PostMapping("/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        File receivedFile = new File("src/main/java/book/img/"+ UUID.randomUUID() +file.getOriginalFilename());
        receivedFile.createNewFile();
        FileOutputStream fout= new FileOutputStream(receivedFile);
        fout.write(file.getBytes());
        fout.close();
        return "http://localhost:8080/image/"+receivedFile.getName();
    }

}
