package com.example.demo.models.BaseApiResponse;

import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

public class BaseApiDelete_UpdateResponse {
    private String message;
    private HttpStatus status;
    private Timestamp time;
    private String path;

    public BaseApiDelete_UpdateResponse() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public BaseApiDelete_UpdateResponse(String message, HttpStatus status, Timestamp time, String path) {
        this.message = message;
        this.status = status;
        this.time = time;
        this.path = path;
    }

    public BaseApiDelete_UpdateResponse(String message, HttpStatus status, Timestamp time) {
        this.message = message;
        this.status = status;
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "BaseApiDelete_UpdateResponse{" +
                "message='" + message + '\'' +
                ", status=" + status +
                ", time=" + time +
                ", path='" + path + '\'' +
                '}';
    }
}
