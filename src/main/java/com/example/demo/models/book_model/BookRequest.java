package com.example.demo.models.book_model;

public class BookRequest
{
    private int categoryId;
    private String title;
    private String author;
    private String description;
    private String thumbnail;

    public BookRequest() {
    }


    public BookRequest(int categoryId, String title, String author, String description, String thumbnail) {
        this.categoryId = categoryId;
        this.title = title;
        this.author = author;
        this.description = description;
        this.thumbnail = thumbnail;
    }


    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String toString() {
        return "BookRequest{" +
                ", categoryId=" + categoryId +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }
}
