package com.example.demo.models.book_model;

public class BookResponse
{
    private int id;
    private int categoryId;
    private String title;
    private String author;
    private String description;
    private String thumbnail;

    public BookResponse() {
    }

    public BookResponse(int id, int categoryId, String title, String author, String description, String thumbnail) {
        this.id = id;
        this.categoryId = categoryId;
        this.title = title;
        this.author = author;
        this.description = description;
        this.thumbnail = thumbnail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }


    @Override
    public String toString() {
        return "BookResponse{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", thubmnail='" + thumbnail + '\'' +
                '}';
    }
}
