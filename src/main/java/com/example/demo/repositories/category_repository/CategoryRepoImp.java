package com.example.demo.repositories.category_repository;

import com.example.demo.models.category_model.CategoryRequest;
import com.example.demo.models.category_model.CategoryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


public class CategoryRepoImp implements CategoryRepository{
    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public List<CategoryResponse> getAllCategory() {
        return categoryRepository.getAllCategory();
    }

    @Override
    public boolean insertNewCategory(CategoryRequest categoryRequest) {
        return categoryRepository.insertNewCategory(categoryRequest);
    }

    @Override
    public boolean deleteCategoryByID(int id) {
        return categoryRepository.deleteCategoryByID(id);
    }

    @Override
    public boolean updateCategoryByID(int id, String  title) {
        return categoryRepository.updateCategoryByID(id, title);
    }
}
