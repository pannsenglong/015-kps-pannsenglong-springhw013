package com.example.demo.repositories.category_repository;

import com.example.demo.database_query_providers.CategoryQueryProvider;
import com.example.demo.models.category_model.CategoryRequest;
import com.example.demo.models.category_model.CategoryResponse;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;

@Repository
public interface CategoryRepository
{
    //@Select("SELECT * FROM tb_category")
    @SelectProvider(value = CategoryQueryProvider.class, method = "selectAllCategory")
    List<CategoryResponse> getAllCategory();

    //@Insert("INSERT INTO tb_category (title) VALUES (#{title})")
    @InsertProvider(value = CategoryQueryProvider.class, method = "insertNewCategory")
    boolean insertNewCategory(CategoryRequest categoryRequest);

    @DeleteProvider(value = CategoryQueryProvider.class, method = "deleteCategoryByID")
    boolean deleteCategoryByID(int id);

    @UpdateProvider(value = CategoryQueryProvider.class, method = "updateCategoryByID")
    boolean updateCategoryByID(int id, String title);


}
