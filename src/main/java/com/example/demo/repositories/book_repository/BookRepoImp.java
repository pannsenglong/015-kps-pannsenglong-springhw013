package com.example.demo.repositories.book_repository;

import com.example.demo.models.book_model.BookRequest;
import com.example.demo.models.book_model.BookResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

//@Repository
public class BookRepoImp implements BookRepository{
    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<BookResponse> getAllBooks() {
        return bookRepository.getAllBooks();
    }

    @Override
    public boolean insertNewBook(BookRequest bookRequest) {
        return bookRepository.insertNewBook(bookRequest);
    }

    @Override
    public boolean deleteBookByID(int id) {
        return bookRepository.deleteBookByID(id);
    }

    @Override
    public boolean updateBookByID(int id,String title, String author, String description, String thumbnail, int category_id) {
        return bookRepository.updateBookByID(id,title, author,description,thumbnail,category_id);
    }

    @Override
    public BookResponse getBookByID(int id) {
        return bookRepository.getBookByID(id);
    }

    @Override
    public List<BookResponse> filterBookByCategory(int categoryId) {
        return bookRepository.filterBookByCategory(categoryId);
    }
}
