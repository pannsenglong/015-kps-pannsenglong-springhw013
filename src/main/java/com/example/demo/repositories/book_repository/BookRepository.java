package com.example.demo.repositories.book_repository;

import com.example.demo.database_query_providers.BookQueryProvider;
import com.example.demo.models.book_model.BookRequest;
import com.example.demo.models.book_model.BookResponse;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository
{
    @SelectProvider(value = BookQueryProvider.class, method = "getAllBooks")
    List<BookResponse> getAllBooks();

    @InsertProvider(value = BookQueryProvider.class, method = "insertNewBook")
    boolean insertNewBook(BookRequest bookRequest);

    @DeleteProvider(value = BookQueryProvider.class, method = "deleteBookByID")
    boolean deleteBookByID(int id);

    @UpdateProvider(value = BookQueryProvider.class, method = "updateBookByID")
    boolean updateBookByID(int id,String title, String author,String description,String thumbnail,int categoryId);

    @SelectProvider(value = BookQueryProvider.class, method = "findBookByID")
    BookResponse getBookByID(int id);

    @SelectProvider(value = BookQueryProvider.class, method = "filterBookByCategory")
    List<BookResponse> filterBookByCategory(int categoryId);
}